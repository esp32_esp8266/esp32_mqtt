# ESP32MQTT MicroPython IoT Weather Station

In the MicroPython IoT Weather Station simulation, the DHT22 sensor. Adjust the temperature/humidity values, and you'll observe the corresponding message appearing on the MQTT Broker, visible in the "Messages" panel.

## Materials

| Materials                   | Quantity |
|-----------------------------|----------|
| ESP32 WiFi Module           | 1        |
| dht22                 | 1        |

## Circuit Diagram

![diagram]()

## Required Library

- None

## Getting Started

### Prerequisites
- [PlatformIO IDE](https://platformio.org/install/ide) or [Arduino IDE](https://www.arduino.cc/en/software)
- ESP32 development board

### Installation
 
1. Clone the repository:
    ```sh
    git clone https://gitlab.com/esp32_esp8266/esp32_mqtt.git
    cd esp32_mqtt/
    ```

2. Open the project in PlatformIO or Arduino IDE.

3. Configure your Wi-Fi credentials in main.cpp:
    ```python
    MQTT_CLIENT_ID = "micropython-weather-demo"
    MQTT_BROKER    = "broker.mqttdashboard.com"
    MQTT_USER      = ""
    MQTT_PASSWORD  = ""
    MQTT_TOPIC     = "wokwi-weather"
    ```

4. Upload the code to your ESP32 board.


### Documentation

Detailed setup and configuration instructions can be found in the [setup guide](docs/setup_guide.md).


## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.
